Analysis
===

This folder has analysis environment for pyDarshan for use with Jupyter.
See https://jupyter-docker-stacks.readthedocs.io/en/latest/using/running.html for more info

## [SWAN](swan.cern.ch)

You can use darshan at CERN's SWAN infrastructure:
at startup, you must use a startup script that specifies the path to `libdarshan-util.so`.
Find your `darshan-util.so` from pip: `pip3 show darshan`, install path + `darshan.libs`

Write the following to SWAN_darshan.sh, then source as $CERNBOX_HOME/SWAN_darshan.sh at startup selection
```sh
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$CERNBOX_HOME/.local/lib/python3.9/site-packages/darshan.libs
```

## Getting the Environment

You can run the image here by pulling:

`docker pull registry.cern.ch/io-benchmarks/dior/analysis`

or by building:

`docker build . -t dior-analysis`

## Running

`docker run -p 8888:8888 dior-analysis`
then open your browser to localhost:8888

## Useful info:

### Using `sudo` in the container:
`docker run -it -e GRANT_SUDO=yes --user root jupyter/minimal-notebook`