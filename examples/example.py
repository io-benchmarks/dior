#!/usr/bin/env python3

import logging
from pathlib import Path
import darshan

logger = logging.getLogger(__name__)

INFILE = Path("example.darshan")
logger.debug("file '%s' selected for parsing" % INFILE)

# experimental required as of 3.3.1 for name_records_summary()
darshan.enable_experimental(verbose=False)
# create report object, read all records, lookup & update name records
report = darshan.DarshanReport(INFILE, read_all=True, lookup_name_records=True)

logger.debug(report.info())

report.name_records_summary()
