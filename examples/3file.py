#!/usr/bin/env python3

# Example writes three different sized files via POSIX

import os

GB1 = 1024 * 1024 * 1024
MB1 = 1024 * 1024
KB1 = 1024

with open("gb1.file", "wb") as fout:
    fout.write(os.urandom(KB1))

with open("gb2.file", "wb") as fout:
    fout.write(os.urandom(MB1))

with open("gb3.file", "wb") as fout:
    fout.write(os.urandom(GB1))
