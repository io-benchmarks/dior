# DIOR

Darshan-to-IOR: a scalable IO benchmark based on IO characteristics of your application

- [DIOR](#dior)
  - [Characterizing large IO binaries](#characterizing-large-io-binaries)
    - [workaround masking the ROOT directory](#workaround-masking-the-root-directory)
    - [increase darshan `RECORD_LIMIT`](#increase-darshan-record_limit)
    - [Report](#report)
    - [Containers](#containers)
  - [Darshan 3.3.1+ Installation](#darshan-331-installation)
    - [Darshan-runtime](#darshan-runtime)
    - [Darshan-util](#darshan-util)
    - [pyDarshan](#pydarshan)
  - [Troubleshooting](#troubleshooting)
    - [Darshan too many entries](#darshan-too-many-entries)
    - [pyDarshan import error](#pydarshan-import-error)

## Characterizing large IO binaries

Darshan has an hard-coded RECORD_LIMIT (1024 files), after which darshan will stop recording IO. ROOT opens thousands of files when loading, so we need to get around this to get an accurate picture:

### workaround masking unwanted logging

`export DARSHAN_EXCLUDE_DIRS="/home/dsouthwi/root/,/lib/,/lib64/,/sys/,/proc/,/usr/,/dev/,/run/,/home/dsouthwi/root_src/,/../,/etc/"`
`export DARSHAN_EXCLUDE_DIRS="/etc/,/dev/,/usr/,/bin/,/boot/,/lib/,/opt/,/sbin/,/sys/,/proc/,/home/"`

### workaround increase darshan `RECORD_LIMIT`

an older branch of darshan has features to get around this hard-coded limit: https://github.com/darshan-hpc/darshan/pull/405 

`export DARSHAN_CONF_PATH=$HOME/darshan.conf`
where darshan.conf:
```sh
# conf file, must be set with DARSHAN_CONF_PATH=~/darshan.conf
# using darshan git on branch snyder/dev-log-filters to override
# hard module record limit(1024)

# KEY       VALUE    MODULES
MAX_RECORDS 50000 *
```

## Running
1. Make the log dir structure: `darshan-mk-log-dirs.pl`
2. Enable darshan: `export DARSHAN_ENABLE_NONMPI=1`
3. (optional) [mask unwanted files](#workaround-masking-unwanted-logging)
4. (optional) Enable DXT: `export DXT_ENABLE_IO_TRACE=1`
3. Run Darshan on arbitrary binary: 
`env LD_PRELOAD=/usr/local/lib/libdarshan.so ~/iotools/h1 -i /data/reference/calibration/h1dstX10~zstd.ntuple -m`

### DXT (darshan Xtended tracing)
enable with both a flag and trigger definition:
`export DXT_ENABLE_IO_TRACE=1 && export DXT_TRIGGER_CONF_PATH=$HOME/darshan.dxt`
where darshan.dxt (interested files all under /data in this example):
```sh
# darshan DXT trace file - required to be set with DXT_TRIGGER_CONF_PATH=~/darshan.dxt
# and DXT enabled with DXT_ENABLE_IO_TRACE=1
FILE ^/data
```
### Report

Unpack the `.darshan` blob with `darshan-parser <infile.darshan>` Can take arguments like `--file-list` (or `--show-incomplete` if you forgot above excludes)

Generate 3-page PDF report with `darshan-job-summary.pl <infile.darshan>`

### Containers

Darshan cannot instrument from outside containers easily, but can be attached as an overlay.
Previous efforts by darshan developers: <https://indico.fnal.gov/event/46114/contributions/201300/attachments/137005/170636/Analyzing-Root-IO-with-Darshan.pdf>

(singularity 3.8+)
`singularity overlay create --size 1024 --create-dir /darshan iotools.sif`

<https://sylabs.io/guides/3.8/user-guide/persistent_overlays.html>

or create with dd in above guide, and then add overlay to sif *(requires root to run*)
`singularity sif add --datatype 4 --partfs 2 --parttype 4 --partarch 2 --groupid 1 iotools_latest.sif overlay.img`
`sudo singularity shell --writable iotools_latest.sif`
Note: Singularity <3.8 requires dd and mke2fs/mkfs.ext3 with `-d` option (not available on popeye)

## Darshan 3.3.1+ Installation

Instructions based on CentOS7 requirements
NB: if using recent repo, build `configure` with `autoreconf -fi`

### Darshan-runtime

```sh

cd darshan-runtime
./configure --with-log-path=/tmp/darshan-logs --with-jobid-env=NONE --without-mpi CC=gcc
make
make install
```

### Darshan-util

report CLI viewing tools, may not be necessary with pyDarshan

```shell=bash
sudo yum install texlive-latex texlive-epstopdf texlive-lastpage texlive-subfigure texlive-multirow texlive-threeparttable texlive-latex-bin-bin-svn perl-Pod-LaTeX bzip2-devel
```

```shell=
cd darshan-util
./configure
make 
make install
```

NB: if you still can't config (missing pdflatex), try the nuclear approach: `yum install texlive-*`
Darshan's docs are bad.

### pyDarshan

```shell=bash
yum install python36-cffi python36-numpy python36-pandas
python3 -m pip install darshan
```

#### pyDarshan devel
```shell=bash
git clone https://github.com/darshan-hpc/darshan.git && cd darshan
git submodule update --init
./prepare.sh
cd darshan-util
./configure
make && make install
cd pydarshan
python3 -m pip install .
```
NB: depends on automake & autoconf - install on OSX with `brew install autoconf automake libtool`
## Troubleshooting

### Darshan too many entries

### pyDarshan import error

darshan installed via pip provides it's own `libdarshan-util.so`, placed in your pip `site-packages/darshan.libs`.
Rather annoyingly, this doesn't work outside of venv, or otherwise unprivileged installs (eg docker).
<https://github.com/darshan-hpc/darshan/issues/552>
renaming the root binary does not fix this.
